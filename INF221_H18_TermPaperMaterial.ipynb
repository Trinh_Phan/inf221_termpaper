{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# INF221 Term Paper Material\n",
    "\n",
    "## Timing Execution\n",
    "\n",
    "### General aspects\n",
    "\n",
    "- Goal of term paper: Compare real-life behavior of sorting algorithms with theoretical expectations.\n",
    "- Perform *measurements*: real-life, physical experiments on computer code and data\n",
    "\n",
    "#### Computer science issues\n",
    "\n",
    "- Correctness of code (it makes no sense to compare incorrect code)\n",
    "- Suitable choice of test data (reflecting test cases and real-life situations)\n",
    "- Correctness of test setup (typical error: only first run sorts test data, all subsequent runs sort sorted data)\n",
    "\n",
    "#### Measurement issues\n",
    "\n",
    "- Resolution and overhead of time taking\n",
    "- Measurement terminology\n",
    "    - Repetition: \n",
    "        - a single independent experiment\n",
    "        - we collect data from multiple repetitions to understand fluctuations in measurment process\n",
    "    - Execution: \n",
    "        - a single call to function to be timed\n",
    "        - repeated many times to get sufficient interval for timing\n",
    "        - each execution must work on pristine data\n",
    "- Measurement process\n",
    "    - Single repetition\n",
    "        1. Start timer\n",
    "        1. Call timed function $n$ times ($n$ executions)\n",
    "        1. Stop timer\n",
    "        1. Report time difference as result of repetition\n",
    "    - Selection of number of executions\n",
    "        1. Start with some $n$, e.g. $n=1$ executions\n",
    "        1. Perform single repetition\n",
    "        1. If repetition took less than $t_{\\text{min}}$, set $n=10n$ and go back to 1.\n",
    "        1. We now have $n_E$, the number of executions required per repetition (may depend on problem and problem size)\n",
    "    - Multiple repetitions\n",
    "        1. Once a suitable number of executions has been found, perform $r$ repetitions\n",
    "        1. Collect results\n",
    "        1. Report\n",
    "- Choosing $t_{\\text{min}}$ and $r$\n",
    "    - Python's `titmeit` uses by default $t_{\\text{min}}=0.2$s, but for more reliable measurements 1s seems more appropriate\n",
    "    - $r$ should be at least 3, but 5 or 7 does not hurt\n",
    "- Integrating results\n",
    "    - In physics experiments, we usually use the mean across repetitions\n",
    "    - Since we want to test the performance of the algorithm, it is common to use the *minimum* across repetitions\n",
    "    - One should still report fluctuations, e.g., as standard deviation or span between minimum and maximum\n",
    "    - Widely scattering values across repetitions indicate strong disturbance by competing processes\n",
    "    - If we have other goals (e.g. data center optimization), median or high percentiles may be more interesting than minimum values (slowest response times)\n",
    "\n",
    "\n",
    "\n",
    "### Python tools\n",
    "- Python provides timing tools through the `timeit` module\n",
    "- See [`timeit` documentation](https://docs.python.org/3/library/timeit.html)\n",
    "- Explore `timeit` code (import in PyCharm and use Ctrl/CMD-B to go to the source)\n",
    "\n",
    "#### The Timeit class\n",
    "\n",
    "- For greatest flexibility, it is best to use the `Timeit` class from `timeit`\n",
    "- See also example solution to Assignment 3\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import timeit\n",
    "import numpy as np\n",
    "import copy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1000 0.3091322939944803\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "[0.31057922200125176,\n",
       " 0.3089788950019283,\n",
       " 0.3098735899984604,\n",
       " 0.3099109460017644,\n",
       " 0.3076200449941098,\n",
       " 0.34214639099809574,\n",
       " 0.30825363299663877]"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "test_data = np.random.random((1000,))\n",
    "\n",
    "clock = timeit.Timer('func(copy(data))',\n",
    "                     globals={'func': sorted, 'data': test_data, \n",
    "                              'copy': copy.copy})\n",
    "\n",
    "executions, t = clock.autorange()\n",
    "print(executions, t)\n",
    "\n",
    "clock.repeat(repeat=7, number=executions)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Notes\n",
    "- We need to explicitly tell the `Timer` class which names from the global namespace should be available in the scope in which the statement to be timed is executed\n",
    "- The time reported is for all executions within a repetition\n",
    "- `autorange()` finds a number of executions that takes longer than 0.2s; inspect its code to find out how to find execution numbers for other limits\n",
    "- For programming efficiency, automate testing as much as possible by defining helper functions!\n",
    "- Add information about your computer!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Formatting notebooks"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Write your notebook as normal\n",
    "- Place files `term_paper.tplx` and `term_paper.bib` into the same directory as the notebook\n",
    "- Edit author, title and abstract in the `tplx` file\n",
    "- Add necessary entries to the `bib` file using [BibTeX](http://www.bibtex.org/Using/) syntax\n",
    "- In text cells in the notebook, you can add citations as follows:\n",
    "\n",
    "        <cite data-cite=\"Corm_Intr_2009\">(Cormen et al, 2009)</cite>\n",
    "        \n",
    "- Convert your notebook to PDF:\n",
    "    1. Convert to LaTeX using\n",
    "    \n",
    "             jupyter nbconvert --to latex --template term_paper.tplx term_paper.ipynb \n",
    "    \n",
    "    1. Run LaTeX and BibTeX using\n",
    "    \n",
    "            pdflatex term_paper\n",
    "            bibtex term_paper\n",
    "            pdflatex term_paper\n",
    "            pdflatex term_paper\n",
    "            \n",
    "- Do this regularly to discover problems early!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction\n",
    "\n",
    "Work describe here is based on  <cite data-cite=\"Corm_Intr_2009\">Cormen et al (2009)</cite>. For a practical benchmarking example see  <cite data-cite=\"Ippe_2017_30\">Ippen et al (2017)</cite> which concerns the [NEST Simulator](www.nest-simulator.org)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
